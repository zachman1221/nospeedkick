package com.zachspong.NoSpeedKick;

import java.util.logging.Logger;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.event.Event;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import com.zachspong.NoSpeedKick.NKPlayerListener;

public class NoSpeedKick extends JavaPlugin {
	  private final NKPlayerListener playerListener = new NKPlayerListener(this);
	  Logger log1 = Logger.getLogger("Minecraft");
	  public final Logger logger = Logger.getLogger("Minecraft");


    public void onDisable() { 
    	PluginDescriptionFile pdFile = this.getDescription();
    	this.logger.info(pdFile.getName() + " version " + pdFile.getVersion() + " is Disabled");
    }

    public void onEnable() {
    	PluginManager pm = getServer().getPluginManager();
    	PluginDescriptionFile pdFile = this.getDescription();
    	this.logger.info(pdFile.getName() + " version " + pdFile.getVersion() + " is Disabled");
	  

      pm.registerEvent(Event.Type.PLAYER_KICK, this.playerListener, Event.Priority.Normal, this);
    }

    public boolean onCommand(CommandSender sender, Command command, String commandLabel, String[] args)
    {
		if(command.getName().toLowerCase().equals("nsk")||command.getName().toLowerCase().equals("nospeedkick")){
	    	PluginDescriptionFile pdFile = this.getDescription();
			sender.sendMessage(ChatColor.GOLD + "NoSpeedKick Version: " + pdFile.getVersion() + " By: " + pdFile.getAuthors());
		}
      return false;
    }
  }